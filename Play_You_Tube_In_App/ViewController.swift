//
//  ViewController.swift
//  Play_You_Tube_In_App
//
//  Created by Amol Tamboli on 22/06/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit
import YoutubeKit

class ViewController: UIViewController {
    
    private var player : YTSwiftyPlayer!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK:- Create new player
        player = YTSwiftyPlayer(frame: CGRect(x: 0, y: 0, width: 640, height: 480), playerVars: [
            .playsInline(true),
            .videoID("WVdpZvA7CyE"),
            .loopVideo(true),
            .showRelatedVideo(true)
        ])
        
        //MARK:- Enable auto playback when video is loaded
        player.autoplay = true
        
        //MARK:- Set player frame
        view = player
        
        //MARK:- Load video player
        player.loadPlayer()
        
        let request = VideoListRequest(part: [.id, .snippet, .contentDetails, .status], filter: .chart)
        
      
         //MARK:- Send request for API
        YoutubeAPI.shared.send(request) { (result) in
            switch result{
            case .success(let response):
                print(response)
            case .failed(let error):
                print(error)
            }
        }
        
    }
    
    
}

